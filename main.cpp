//prosty program słuzacy do całkowania metodą Simpsona.
#include <iomanip>
#include <iostream>
#include <cstdlib>

using namespace std;

double funkcja(double x){
    return(x*x+2*x);
}

int main(){
    const int N = 100;
    double xpoczatkowe,xkoncowe,wartFunk,sumaWart,dx,x;
    //wyjasnienie zmiennych
    //xpoczatkowe - wartosc poczatku przedziału całkowania
    //xkoncowe - wartosc konca przedziału całkowania
    //wartFunk - przybliżona wartosc funkcji w granicach przedziału całkowania
    //sumaWart - suma wartosci funkcji w punktach srodkowych miedzy punktami
    //dx - odleglosc miedzy dwoma sasiednimi punktami
    //x - pozycja punktu
    //licznik - licznik punktow podzialowych
    int i;
    cout<<"prosze podac poczatek przedziału całkowania: ";
    cin>>xpoczatkowe;
    cout<<"prosze podac koniec przedziału całkowania: ";
    cin>>xkoncowe;
    wartFunk = 0;
    sumaWart = 0;
    dx = (xkoncowe-xpoczatkowe)/N;
    for(i=1;i<=N;i++){
        x = xpoczatkowe + i * dx;
        sumaWart += funkcja(x-dx/2);
        if(i<N){
            wartFunk+=funkcja(x);
        }
    }
    wartFunk = dx/6 * (funkcja(xpoczatkowe)+funkcja(xkoncowe)+2*wartFunk+4*sumaWart);
    cout<<"wartosc calki wynosi: "<<setw(8)<<wartFunk<<endl;
    return 0;
}